const express = require('express');
const app = express();
const path = require('path');

app.use(express.json());

const users = ['Hermione Granger', 'Harry Potter', 'Draco Malfoy', 'Ron Weasley'];

// GET ALL
app.get('/users', (req, res) => {
    return res.json(users);
});  

app.listen(3000, () => {
    console.log('Server started at http://localhost:' + 3000);
});

exports.app = app;

# Test Automation Essentials Assignment TESTE

A small application to implement the different kinds of tests presented in Test Automation Essentials.

## Overview
This is a simple project that adds the user's name and age.<br>
For testing purposes only.

## Tech Stack
The following tools were used in the construction of the application:

**Test:** jest<br>
**Broad Integration Test:** supertest<br>
**Application bundler:** Webpack<br>
**Node library:** Puppeteer

## How to run
```bash
# Install dependencies
$ npm i

# Run Tests
$ npm test

# Run the project in development mode
$ npm start

```

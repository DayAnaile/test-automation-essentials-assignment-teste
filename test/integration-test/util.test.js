const request = require('supertest');
const { checkAndGenerate } = require('../../src/util');
const { app } = require('../../server');

// narrow
test('should generate a valid text output', () => {
  const text = checkAndGenerate('Name', 41);
  expect(text).toBe('Name (41 years old)');
});

// broad
test('GET /users', async () => {
  const call = await request(app).get('/users').expect(200)
  expect(call.statusCode).toEqual(200)
})



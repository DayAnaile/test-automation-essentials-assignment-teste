const puppeteer = require('puppeteer');

test('should create an element with text and correct class', async () => {
  const browser = await puppeteer.launch({
    headless: true,
  });


  const page = await browser.newPage();
  await page.goto(
    'file:////Users/dayany.santos/Documents/ACP2P/test-automation-essentials-assignment-teste/src/index.html'
  );


  await page.click('input#name');
  await page.type('input#name', 'Name');
  await page.click('input#age');
  await page.type('input#age', '41');
  await page.click('#btnAddUser');

  const finalText = await page.$eval('.user-item', el => el.textContent);
  expect(finalText).toBe('Name (41 years old)');

}, 10000);

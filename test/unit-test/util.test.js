const { generateText } = require('../../src/util');

test('should output name and age', () => {
  const text = generateText('Name', 41);
  expect(text).toBe('Name (41 years old)');
});

test('should output data-less text', () => {
  const text = generateText('', null);
  expect(text).toBe(` (${null} years old)`);
});

